# we won't deal with the non-Darkfish RDoc in older rubies, it has frames :<
if (!defined?(RUBY_ENGINE) || RUBY_ENGINE == "ruby") && \
   RUBY_VERSION.to_f <= 1.8
  require 'rubygems'
end

# we never want the rdoc 2.5.x from Ruby 1.9.2
gem 'rdoc', '~> 3.9.4'
require 'rdoc/rdoc'

class Wrongdoc::Rdoc
  include Wrongdoc::RdocOptions
  include Wrongdoc::ParseXML

  def initialize(opts)
    @rdoc_uri = URI.parse(opts[:rdoc_url])
    @cgit_uri = URI.parse(opts[:cgit_url])
  end

  def run(argv = [])
    rdoc(argv)
    add_atom("doc/ChangeLog.html", cgit_atom_uri)
    add_atom("doc/NEWS.html", news_atom_uri)
    add_atom("doc/README.html", news_atom_uri)

    # the stock RDoc index page layout lacks a vertical sidebar full of links
    rdoc_index = "doc/rdoc_index.html"
    File.exist?(rdoc_index) and File.unlink(rdoc_index)
    File.rename("doc/index.html", rdoc_index)
    File.link("doc/README.html", "doc/index.html")
  end

  def rdoc(argv)
    r = RDoc::RDoc.new
    r.document(rdoc_options.concat(argv))
  end

  def add_atom(path, atom_uri)
    File.open(path, "a+") do |fp|
      doc = parse_xml(fp.read)
      doc.search("title").each { |t|
        t.add_next_sibling(atom_node(doc, atom_uri))
      }
      fp.truncate 0
      fp.write doc.to_xhtml
    end
  end

  def cgit_atom_uri
    uri = @cgit_uri.dup
    uri.path += "/atom/"
    uri.query = "h=master"
    uri
  end

  def news_atom_uri
    uri = @rdoc_uri.dup
    uri.path += "NEWS.atom.xml"
    uri
  end

  def atom_node(doc, uri, title = 'Atom feed')
    link = Nokogiri::XML::Node.new('link', doc)
    link['rel'] = 'alternate'
    link['title'] = title
    link['href'] = uri.to_s
    link['type'] = 'application/atom+xml'
    link
  end
end
