require 'tempfile'
require 'uri'
require 'yaml'
require 'tidy_ffi'
require 'nokogiri'

module Wrongdoc

  autoload :Readme, 'wrongdoc/readme'
  autoload :History, 'wrongdoc/history'
  autoload :Release, 'wrongdoc/release'

  autoload :Changelog, 'wrongdoc/changelog'
  autoload :NewsRdoc, 'wrongdoc/news_rdoc'
  autoload :NewsAtom, 'wrongdoc/news_atom'
  autoload :ParseXML, 'wrongdoc/parse_xml'

  autoload :Prepare, 'wrongdoc/prepare'
  autoload :Rdoc, 'wrongdoc/rdoc'
  autoload :Merge, 'wrongdoc/merge'
  autoload :Final, 'wrongdoc/final'

  autoload :Gemspec, 'wrongdoc/gemspec'
  autoload :RdocOptions, 'wrongdoc/rdoc_options'

  def self.config(path = ".wrongdoc.yml")
    File.exist?(path) or abort "#{path} not found in current directory"
    opts = YAML.load(File.read(path))
    opts.keys.each { |k| opts[k.to_sym] = opts.delete(k) } # symbolize keys
    opts
  end
end
