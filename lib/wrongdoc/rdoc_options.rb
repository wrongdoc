module Wrongdoc::RdocOptions
  include Wrongdoc::Readme

  def rdoc_options
    webcvs = URI.parse(Wrongdoc.config[:cgit_url])
    webcvs.path += "/tree"
    webcvs = "#{webcvs}/%s"
    _, _, title = readme_metadata
    [ '-t', title, '-W', webcvs ]
  end
end
