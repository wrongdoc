# -*- encoding: binary -*-
ENV["VERSION"] or abort "VERSION= must be specified"
manifest = File.readlines('.manifest').map! { |x| x.chomp! }
$LOAD_PATH << 'lib'
require 'wrongdoc'
extend Wrongdoc::Gemspec
name, summary, title = readme_metadata

Gem::Specification.new do |s|
  s.name = %q{wrongdoc}
  s.version = ENV["VERSION"].dup
  s.authors = ["#{name} hackers"]
  s.description = readme_description
  s.email = %q{wrongdoc@librelist.org}
  s.executables = %w(wrongdoc)
  s.extra_rdoc_files = extra_rdoc_files(manifest)
  s.files = manifest
  s.homepage = Wrongdoc.config[:rdoc_url]
  s.summary = summary
  s.rdoc_options = rdoc_options
  s.add_dependency(%q<nokogiri>, ['~> 1.5'])
  s.add_dependency(%q<tidy_ffi>, ['~> 0.1', '>= 0.1.5'])
  s.add_dependency(%q<rdoc>, ['~> 3.9', '>= 3.9'])
  s.licenses = %w(GPLv3+)
end
