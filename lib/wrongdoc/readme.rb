# helpers for parsing the top-level README file (no suffix support :P)
module Wrongdoc::Readme

  # returns a one-paragraph summary from the README
  def readme_description
    File.read("README").split(/\n\n/)[1]
  end

  # parses the README file in the top-level directory for project metadata
  def readme_metadata
    l = File.readlines("README")[0].strip!
    l.gsub!(/^=\s+/, '') or abort "#{l.inspect} doesn't start with '='"
    title = l.dup
    if l.gsub!(/^(\w+\!)\s+/, '') # Rainbows!
      return $1, l, title
    else
      return (l.split(/\s*[:-]\s*/, 2)).push(title)
    end
  end
end
