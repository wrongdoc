module Wrongdoc::NewsAtom
  include Wrongdoc::History
  include Wrongdoc::Readme

  # generates an Atom feed based on git tags in the document directory
  def news_atom
    project_name, short_desc, _ = readme_metadata
    new_tags = tags[0,10]
    atom_uri = @rdoc_uri.dup
    atom_uri.path += "NEWS.atom.xml"
    news_uri = @rdoc_uri.dup
    news_uri.path += "NEWS.html"
    doc = Nokogiri::XML::Builder.new {
      feed :xmlns => "http://www.w3.org/2005/Atom" do
        id! atom_uri.to_s
        title "#{project_name} news"
        subtitle short_desc
        link! :rel => 'alternate', :type => 'text/html', :href => news_uri.to_s
        updated new_tags.empty? ? '1970-01-01:00:00:00Z' : new_tags[0][:time]
        new_tags.each do |tag|
          entry {
            title tag[:subject]
            updated tag[:time]
            published tag[:time]
            author {
              name tag[:tagger_name]
              email tag[:tagger_email]
            }
            uri = tag_uri(tag[:tag]).to_s
            link! :rel => "alternate", :type => "text/html", :href => uri
            id! uri
            message_only = tag[:body].split(/\n.+\(\d+\):\n {6}/s)[0].strip
            content({:type =>:text}, message_only)
            content(:type =>:xhtml) { pre tag[:body] }
          }
        end
      end
    }
    fpath = "doc/NEWS.atom.xml"
    File.open(fpath, "w") { |fp| fp.write doc.to_xml(:indent => 0) }
    unless new_tags.empty?
      time = new_tags[0][:ruby_time]
      File.utime(time, time, fpath)
    end
  end
end
