module Wrongdoc::Release
  self.extend Wrongdoc::History

  def self.changes(io)
    vtags = tags.map { |tag| tag[:tag] =~ /\Av/ and tag[:tag] }.sort
    cmds = []
    if vtags.empty?
      cmds << %w(git log)
    else
      version = vtags[-1]
      prev = vtags[vtags.index(version) - 1]
      if prev
        cmds << [ 'git', 'diff', '--stat', prev, version ]
        cmds << [ 'git', 'log', "#{prev}..#{version}" ]
      else
        cmds << [ 'git', 'log', version ]
      end
    end

    io.sync = true
    cmds.each_with_index do |cmd,i|
      i > 0 and io.puts
      _, status = Process.waitpid2(fork do
        if io.fileno != $stdout.fileno
          $stdout.reopen(io)
          io.close
        end
        exec(*cmd)
      end)
      status.success? or abort status.inspect
    end
  end

  def self.notes(io, opts)
    spec = Gem::Specification.load(Dir['*.gemspec'][0])
    _, subject, body = `git cat-file tag v#{spec.version}`.split(/\n\n/, 3)
    io.puts subject
    io.puts
    io.puts spec.description.strip
    io.puts
    io.puts "* #{spec.homepage}"
    io.puts "* #{spec.email}"
    io.puts "* #{opts[:git_url] || opts[:cgit_url]}"
    io.puts "* #{spec.homepage}NEWS.atom.xml"

    io.print "\nChanges:\n\n"
    io.puts body
  end
end
