#!/bin/sh
# self-hosting version used for development
set -e
prefix="$(git rev-parse --show-toplevel)"
exec ruby -I $prefix/lib $prefix/bin/wrongdoc "$@"
