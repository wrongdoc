class Wrongdoc::Merge
  include Wrongdoc::ParseXML

  def initialize(opts)
    @merge_html = opts[:merge_html] || {}
  end

  def run
    @merge_html.each do |file, source|
      rdoc_html = "doc/#{file}.html"
      src = Nokogiri::XML.fragment(File.read(source))
      File.open(rdoc_html, "a+") { |fp|
        doc = parse_xml(fp.read)
        doc.search("div#documentation")[0].add_child(src)
        fp.truncate 0
        fp.write doc.to_xhtml
      }
    end
  end
end
