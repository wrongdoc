module Wrongdoc::ParseXML
  def parse_xml(str)
    opts = {
      :input_encoding => 'utf8',
      :output_encoding => 'utf8',
      :wrap => 0,
      :tidy_mark => false,
    }
    Nokogiri::XML(TidyFFI::Tidy.new(str, opts).clean)
  end
end
