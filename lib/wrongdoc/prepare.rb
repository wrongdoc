class Wrongdoc::Prepare
  include Wrongdoc::NewsRdoc
  include Wrongdoc::Changelog
  include Wrongdoc::Readme

  def initialize(opts)
    @rdoc_uri = URI.parse(opts[:rdoc_url])
    @cgit_uri = URI.parse(opts[:cgit_url])
    @changelog_start = opts[:changelog_start]
    @name, @short_desc = readme_metadata
  end

  def run
    news_rdoc
    changelog
  end
end
